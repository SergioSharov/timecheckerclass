<?php

namespace SergioSharov\TimeCheckerClass\Service;

class TimeCheckService
{
    private $time;

    public function __construct($timezone)
    {
        $this->time = new \DateTime($timezone);
    }


    public function getCurrentTime()
    {
        return $this->time->format("l, d-M-Y H:i:s T");
    }

}